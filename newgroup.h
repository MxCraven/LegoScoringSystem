#ifndef NEWGROUP_H
#define NEWGROUP_H

#include <QDialog>

namespace Ui {
class NewGroup;
}

class NewGroup : public QDialog
{
    Q_OBJECT

public:
    explicit NewGroup(QWidget *parent = 0);
    ~NewGroup();

private slots:
    int brickCount();
    int height();

    void on_pushButton_Add_clicked();

    void on_pushButton_Cancel_clicked();

private:
    Ui::NewGroup *ui;
};

#endif // NEWGROUP_H
